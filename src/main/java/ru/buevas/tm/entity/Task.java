package ru.buevas.tm.entity;

/**
 * Сущность для хранения инфомации о задаче
 */
public class Task {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    /**
     * Конструктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор по имени задачи
     *
     * @param name наименование создаваемой задачи
     */
    public Task(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("{Task %d - %s}", id, name);
    }
}

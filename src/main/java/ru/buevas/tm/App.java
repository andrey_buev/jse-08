package ru.buevas.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import ru.buevas.tm.constant.TerminalConst.CliCommands;
import ru.buevas.tm.dao.ProjectDao;
import ru.buevas.tm.dao.TaskDao;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.entity.Task;

/**
 * Основной класс приложения
 *
 * @author Andrey Buev
 */
public class App {

    private static final ProjectDao projectDao = new ProjectDao();

    private static final TaskDao taskDao = new TaskDao();

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Точка входа в приложение
     *
     * @param args - параметры командной строки, переданные при старте приложения
     */
    public static void main(String[] args) {
        printWelcome();
        run(args);
        process();
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args - массив аргументов
     */
    public static void run(final String[] args) {
        if (args == null || args.length < 1) {
            return;
        }

        final String command = args[0];
        System.exit(execCommand(command));
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */
    public static void process() {
        String command = "";
        while (!CliCommands.EXIT.equals(command)) {
            try {
                command = reader.readLine();
                execCommand(command);
                System.out.println();
            } catch (IOException e) {
                printError(e.getMessage());
            }
        }
    }

    /**
     * Выполнение команды
     *
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */
    public static int execCommand(final String command) {
        if (command == null || command.isEmpty()) {
            return -1;
        }
        switch (command) {
            case CliCommands.VERSION: {
                return printVersion();
            }
            case CliCommands.ABOUT: {
                return printAbout();
            }
            case CliCommands.HELP: {
                return printHelp();
            }
            case CliCommands.EXIT: {
                return exit();
            }
            case CliCommands.PROJECT_CREATE: {
                return createProject();
            }
            case CliCommands.PROJECT_LIST: {
                return listProject();
            }
            case CliCommands.PROJECT_UPDATE_BY_INDEX: {
                return updateProjectByIndex();
            }
            case CliCommands.PROJECT_UPDATE_BY_ID: {
                return updateProjectById();
            }
            case CliCommands.PROJECT_VIEW_BY_INDEX: {
                return viewProjectByIndex();
            }
            case CliCommands.PROJECT_VIEW_BY_ID: {
                return viewProjectById();
            }
            case CliCommands.PROJECT_VIEW_BY_NAME: {
                return viewProjectByName();
            }
            case CliCommands.PROJECT_REMOVE_BY_INDEX: {
                return removeProjectByIndex();
            }
            case CliCommands.PROJECT_REMOVE_BY_ID: {
                return removeProjectById();
            }
            case CliCommands.PROJECT_REMOVE_BY_NAME: {
                return removeProjectByName();
            }
            case CliCommands.PROJECT_CLEAR: {
                return clearProject();
            }
            case CliCommands.TASK_CREATE: {
                return createTask();
            }
            case CliCommands.TASK_LIST: {
                return listTask();
            }
            case CliCommands.TASK_UPDATE_BY_INDEX: {
                return updateTaskByIndex();
            }
            case CliCommands.TASK_UPDATE_BY_ID: {
                return updateTaskById();
            }
            case CliCommands.TASK_VIEW_BY_INDEX: {
                return viewTaskByIndex();
            }
            case CliCommands.TASK_VIEW_BY_ID: {
                return viewTaskById();
            }
            case CliCommands.TASK_VIEW_BY_NAME: {
                return viewTaskByName();
            }
            case CliCommands.TASK_REMOVE_BY_INDEX: {
                return removeTaskByIndex();
            }
            case CliCommands.TASK_REMOVE_BY_ID: {
                return removeTaskById();
            }
            case CliCommands.TASK_REMOVE_BY_NAME: {
                return removeTaskByName();
            }
            case CliCommands.TASK_CLEAR: {
                return clearTask();
            }
            default: {
                return printError("Unknown parameter");
            }
        }
    }

    /**
     * Вывод на экран приветствия
     */
    public static void printWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Обработка команды вывода версии приложения
     *
     * @return 0
     */
    public static int printVersion() {
        System.out.println("Task Manager version 1.0.0");
        return 0;
    }

    /**
     * Обработка команды вывода сведений о разработчике
     *
     * @return 0
     */
    public static int printAbout() {
        System.out.println("Developer: Andrey Buev (buev_as@mail.ru)");
        return 0;
    }

    /**
     * Обработка команды вывода справки
     *
     * @return 0
     */
    public static int printHelp() {
        System.out.printf("%s - Display version information%n", CliCommands.VERSION);
        System.out.printf("%s - Display version information%n", CliCommands.ABOUT);
        System.out.printf("%s - Display version information%n", CliCommands.HELP);
        System.out.printf("%s - Display version information%n", CliCommands.EXIT);
        System.out.println();
        System.out.printf("%s - Create new project%n", CliCommands.PROJECT_CREATE);
        System.out.printf("%s - Display list of projects%n", CliCommands.PROJECT_LIST);
        System.out.printf("%s - Update project by index in list%n", CliCommands.PROJECT_UPDATE_BY_INDEX);
        System.out.printf("%s - Update project by id%n", CliCommands.PROJECT_UPDATE_BY_ID);
        System.out.printf("%s - View project by index in list%n", CliCommands.PROJECT_VIEW_BY_INDEX);
        System.out.printf("%s - View project by id%n", CliCommands.PROJECT_VIEW_BY_ID);
        System.out.printf("%s - View project by name%n", CliCommands.PROJECT_VIEW_BY_NAME);
        System.out.printf("%s - Remove project by index in list%n", CliCommands.PROJECT_REMOVE_BY_INDEX);
        System.out.printf("%s - Remove project by id%n", CliCommands.PROJECT_REMOVE_BY_ID);
        System.out.printf("%s - Remove project by name%n", CliCommands.PROJECT_REMOVE_BY_NAME);
        System.out.printf("%s - Clear list of projects%n", CliCommands.PROJECT_CLEAR);
        System.out.println();
        System.out.printf("%s - Create new task%n", CliCommands.TASK_CREATE);
        System.out.printf("%s - Display list of tasks%n", CliCommands.TASK_LIST);
        System.out.printf("%s - Update task by index in list%n", CliCommands.TASK_UPDATE_BY_INDEX);
        System.out.printf("%s - Update task by id%n", CliCommands.TASK_UPDATE_BY_ID);
        System.out.printf("%s - View task by index in list%n", CliCommands.TASK_VIEW_BY_INDEX);
        System.out.printf("%s - View task by id%n", CliCommands.TASK_VIEW_BY_ID);
        System.out.printf("%s - View task by name%n", CliCommands.TASK_VIEW_BY_NAME);
        System.out.printf("%s - Remove task by index in list%n", CliCommands.TASK_REMOVE_BY_INDEX);
        System.out.printf("%s - Remove task by id%n", CliCommands.TASK_REMOVE_BY_ID);
        System.out.printf("%s - Remove task by name%n", CliCommands.TASK_REMOVE_BY_NAME);
        System.out.printf("%s - Clear list of tasks%n", CliCommands.TASK_CLEAR);
        return 0;
    }

    /**
     * Обработка команды создания проекта
     */
    public static int createProject() {
        System.out.println("[CREATE PROJECT]");
        try {
            System.out.print("Enter project name: ");
            final String name = reader.readLine();
            System.out.print("Enter project description: ");
            final String description = reader.readLine();
            projectDao.create(name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды вывода списка проектов
     */
    public static int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println("Available projects:");
        List<Project> projects = projectDao.findAll();
        if (projects.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (Project project : projects) {
                System.out.printf("%d. %s - %s%n", index, project.getId(), project.getName());
                index++;
            }
        }
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды обновления проекта по номеру в списке
     */
    public static int updateProjectByIndex() {
        System.out.println("[Update PROJECT]");
        try {
            System.out.print("Enter project index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Project project = projectDao.findByIndex(index);
            if (project == null) {
                printError("Project not found");
                return 0;
            }
            System.out.print("Enter project name: ");
            final String name = reader.readLine();
            System.out.print("Enter project description: ");
            final String description = reader.readLine();
            projectDao.update(project.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды обновления проекта по идентификатору
     */
    public static int updateProjectById() {
        System.out.println("[Update PROJECT]");
        try {
            System.out.print("Enter project id: ");
            Long id = Long.parseLong(reader.readLine());
            Project project = projectDao.findById(id);
            if (project == null) {
                printError("Project not found");
                return 0;
            }
            System.out.print("Enter project name: ");
            final String name = reader.readLine();
            System.out.print("Enter project description: ");
            final String description = reader.readLine();
            projectDao.update(project.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Вывод на экран информации о проекте
     *
     * @param project - проект
     * @see Project
     */
    private static void printProject(final Project project) {
        if (project == null) {
            return;
        }
        System.out.println("[VIEW PROJECT]");
        System.out.println("id: " + project.getId());
        System.out.println("name: " + project.getName());
        System.out.println("description: " + project.getDescription());
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды отображения проекта по номеру в списке
     */
    public static int viewProjectByIndex() {
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Project project = projectDao.findByIndex(index);
            if (project != null) {
                printProject(project);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения проекта по идентификатору
     */
    public static int viewProjectById() {
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Project project = projectDao.findById(id);
            if (project != null) {
                printProject(project);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения проекта по имени
     */
    public static int viewProjectByName() {
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Project project = projectDao.findByName(name);
            if (project != null) {
                printProject(project);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удалдения проекта по номеру в списке
     */
    public static int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Project project = projectDao.removeByIndex(index);
            if (project != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удалдения проекта по идентификатору
     */
    public static int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Project project = projectDao.removeById(id);
            if (project != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удалдения проекта по имени
     */
    public static int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Project project = projectDao.removeByName(name);
            if (project != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отчистки списка проектов
     */
    public static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDao.clear();
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды создания задачи
     */
    public static int createTask() {
        System.out.println("[CREATE TASK]");
        try {
            System.out.print("Enter task name: ");
            final String name = reader.readLine();
            System.out.print("Enter task description: ");
            final String description = reader.readLine();
            taskDao.create(name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды вывода списка задач
     */
    public static int listTask() {
        System.out.println("[LIST TASK]");
        System.out.println("Available tasks:");
        List<Task> tasks = taskDao.findAll();
        if (tasks.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (Task task : tasks) {
                System.out.printf("%d. %s - %s%n", index, task.getId(), task.getName());
                index++;
            }
        }
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды обновления задачи по номеру в списке
     */
    public static int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        try {
            System.out.print("Enter task index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskDao.findByIndex(index);
            if (task == null) {
                printError("Task not found");
                return 0;
            }
            System.out.print("Enter task name: ");
            final String name = reader.readLine();
            System.out.print("Enter task description: ");
            final String description = reader.readLine();
            taskDao.update(task.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды обновления задачи по номеру в списке
     */
    public static int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        try {
            System.out.print("Enter task id: ");
            Long id = Long.parseLong(reader.readLine());
            Task task = taskDao.findById(id);
            if (task == null) {
                printError("Task not found");
                return 0;
            }
            System.out.print("Enter task name: ");
            final String name = reader.readLine();
            System.out.print("Enter task description: ");
            final String description = reader.readLine();
            taskDao.update(task.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Вывод на экран информации о задаче
     *
     * @param task - задача
     * @see Task
     */
    private static void printTask(final Task task) {
        if (task == null) {
            return;
        }
        System.out.println("[VIEW TASK]");
        System.out.println("id: " + task.getId());
        System.out.println("name: " + task.getName());
        System.out.println("description: " + task.getDescription());
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды отображения задачи по номеру в списке
     */
    public static int viewTaskByIndex() {
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskDao.findByIndex(index);
            if (task != null) {
                printTask(task);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения задачи по ижентификатору
     */
    public static int viewTaskById() {
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Task task = taskDao.findById(id);
            if (task != null) {
                printTask(task);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения задачи по имени
     */
    public static int viewTaskByName() {
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Task task = taskDao.findByName(name);
            if (task != null) {
                printTask(task);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удалдения проекта по номеру в списке
     */
    private static int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskDao.removeByIndex(index);
            if (task != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удалдения проекта по идентификатору
     */
    private static int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Task task = taskDao.removeById(id);
            if (task != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удалдения проекта по имени
     */
    private static int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Task task = taskDao.removeByName(name);
            if (task != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отчистки списка задач
     */
    public static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDao.clear();
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @param message - сообщение
     * @return -1
     */
    public static int printError(final String message) {
        System.out.println("[ERROR]");
        System.out.println(message);
        return -1;
    }

    /**
     * Завершение работы приложения
     *
     * @return 0
     */
    public static int exit() {
        System.exit(0);
        return 0;
    }
}

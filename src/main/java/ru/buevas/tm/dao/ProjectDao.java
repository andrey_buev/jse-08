package ru.buevas.tm.dao;

import java.util.ArrayList;
import java.util.List;
import ru.buevas.tm.entity.Project;

public class ProjectDao {

    /**
     * Коллекция, хранящая проекты
     */
    private final List<Project> projects = new ArrayList<>();

    /**
     * Создание проекта
     *
     * @param name - имя проекта
     * @return созданный проект
     * @see Project
     */
    public Project create(final String name) {
        final Project newProject = new Project(name);
        projects.add(newProject);
        return newProject;
    }

    /**
     * Создание проекта
     *
     * @param name - имя проекта
     * @param description - описание проекта
     * @return созданный проект
     * @see Project
     */
    public Project create(final String name, final String description) {
        final Project newProject = new Project();
        newProject.setName(name);
        newProject.setDescription(description);
        projects.add(newProject);
        return newProject;
    }

    /**
     * Получение всех проектов
     *
     * @return коллекция проектов
     */
    public List<Project> findAll() {
        return projects;
    }

    /**
     * Обновление информации о проекте
     *
     * @param id - идентификатор проекта
     * @param name - имя проекта
     * @param description - описание проекта
     * @return обновленный проект
     */
    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Получение проекта по номеру в списке
     *
     * @param index - номер в списке
     * @return null если проект не найден, иначе проект
     */
    public Project findByIndex(int index) {
        if (checkIndex(index)) {
            return projects.get(index);
        }
        return null;
    }

    /**
     * Получение проекта по идентификатору
     *
     * @param id - идентификатор проекта
     * @return null если проект не найден, иначе проект
     */
    public Project findById(final Long id) {
        for (Project project : projects) {
            if (project.getId().equals(id)) {
                return project;
            }
        }
        return null;
    }

    /**
     * Получение проекта по имени
     *
     * @param name - имя проекта
     * @return null если проект не найден, иначе проект
     */
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                return project;
            }
        }
        return null;
    }

    /**
     * Удаление проекта по номеру в списке
     *
     * @param index - номер в списке
     * @return null если удаляемый проект не найден, иначе удаленный проект
     */
    public Project removeByIndex(int index) {
        Project project = findByIndex(index);
        if (project != null) {
            projects.remove(project);
            return project;
        }
        return null;
    }

    /**
     * Удаление проекта по идентификатору
     *
     * @param id - идентификатор проекта
     * @return null если удаляемый проект не найден, иначе удаленный проект
     */
    public Project removeById(Long id) {
        Project project = findById(id);
        if (project != null) {
            projects.remove(project);
            return project;
        }
        return null;
    }

    /**
     * Удаление проекта по имени
     *
     * @param name - имя проекта
     * @return null если удаляемый проект не найден, иначе удаленный проект
     */
    public Project removeByName(String name) {
        Project project = findByName(name);
        if (project != null) {
            projects.remove(project);
            return project;
        }
        return null;
    }

    /**
     * Очистка коллекции проектов
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Проверка индекса на предмет выхода за пределы коллекции проектов
     *
     * @param index - проверяемый индекс
     * @return true - индекс корректен, false - индекс выходит за пределы коллекции
     */
    private boolean checkIndex(int index) {
        int lastIndex = projects.size() - 1;
        return index >= 0 && index <= lastIndex;
    }
}

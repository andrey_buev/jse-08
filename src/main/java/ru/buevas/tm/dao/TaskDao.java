package ru.buevas.tm.dao;

import java.util.ArrayList;
import java.util.List;
import ru.buevas.tm.entity.Task;

public class TaskDao {

    /**
     * Коллекция, хранящая задачи
     */
    private final List<Task> tasks = new ArrayList<>();

    /**
     * Создание задачи
     *
     * @param name - имя задачи
     * @return созданная задача
     * @see Task
     */
    public Task create(final String name) {
        final Task newTask = new Task(name);
        tasks.add(newTask);
        return newTask;
    }

    /**
     * Создание задачи
     *
     * @param name - имя задачи
     * @param description - описание задачи
     * @return созданная задача
     * @see Task
     */
    public Task create(final String name, final String description) {
        final Task newTask = new Task();
        newTask.setName(name);
        newTask.setDescription(description);
        tasks.add(newTask);
        return newTask;
    }

    /**
     * Получение всех задач
     *
     * @return коллекция задач
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Обновление информации о задаче
     *
     * @param id - идентификатор задачи
     * @param name - имя задачи
     * @param description - описание задачи
     * @return обновленная задача
     */
    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Получение задачи по номеру в списке
     *
     * @param index - номер в списке
     * @return null если задача не найдена, иначе задача
     */
    public Task findByIndex(int index) {
        if (checkIndex(index)) {
            return tasks.get(index);
        }
        return null;
    }

    /**
     * Получение задачи по идентификатору
     *
     * @param id - идентификатор задачи
     * @return null если задача не найдена, иначе задача
     */
    public Task findById(final Long id) {
        for (Task task : tasks) {
            if (task.getId().equals(id)) {
                return task;
            }
        }
        return null;
    }

    /**
     * Получение задачи по имени
     *
     * @param name - имя задачи
     * @return null если задача не найдена, иначе задача
     */
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        for (Task task : tasks) {
            if (task.getName().equals(name)) {
                return task;
            }
        }
        return null;
    }

    /**
     * Удаление задачи по номеру в списке
     *
     * @param index - номер в списке
     * @return null если удаляемая задача не найдена, иначе удаленная задача
     */
    public Task removeByIndex(int index) {
        Task task = findByIndex(index);
        if (task != null) {
            tasks.remove(task);
            return task;
        }
        return null;
    }

    /**
     * Удаление задачи по идентификатору
     *
     * @param id - идентификатор задачи
     * @return null если удаляемая задача не найдена, иначе удаленная задача
     */
    public Task removeById(Long id) {
        Task task = findById(id);
        if (task != null) {
            tasks.remove(task);
            return task;
        }
        return null;
    }

    /**
     * Удаление задачи по имени
     *
     * @param name - имя задачи
     * @return null если удаляемая задача не найдена, иначе удаленная задача
     */
    public Task removeByName(String name) {
        Task task = findByName(name);
        if (task != null) {
            tasks.remove(task);
            return task;
        }
        return null;
    }

    /**
     * Очистка коллекции задач
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Проверка индекса на предмет выхода за пределы коллекции задач
     *
     * @param index - проверяемый индекс
     * @return true - индекс корректен, false - индекс выходит за пределы коллекции
     */
    private boolean checkIndex(int index) {
        int lastIndex = tasks.size() - 1;
        return index >= 0 && index <= lastIndex;
    }
}
